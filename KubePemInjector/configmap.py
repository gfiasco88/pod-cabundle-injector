# operation on configmap
from typing import Dict, List, Optional
from copy import deepcopy
import kubernetes
from kubernetes.client.models import V1ConfigMap, V1ObjectMeta
from .namespace import InjectNamespace
from . import exceptions


class SyncConfigMap(InjectNamespace):
    '''
    class to syncronize Kubernetes V1ConfigMap across NameSpaces
    relationship between namespaces set as master/slave
    '''
    def __init__(self, cm_label: str = 'pemsync'):
        self.cm_label = cm_label
        super().__init__()

    def get_managed_cm(
            self) -> Dict[Optional[str], List[Optional[V1ConfigMap]]]:
        '''
        generates a list of configmaps present in each master namespace
        '''
        configmaps = {}
        try:
            for each_ns in self.managed_namespaces(selector='master'):
                configmaps[each_ns] = self.api.list_namespaced_config_map(
                    each_ns, label_selector=self.cm_label)
            return configmaps
        except kubernetes.client.rest.ApiException as e:
            print(f"Exception CoreV1Api->list_namespaced_config_map: {e}\n")
            raise

    def is_cm_present(
        self,
        name: str,
        namespace: str,
    ) -> bool:
        try:
            self.api.read_namespaced_config_map(name, namespace)
            return True
        except kubernetes.client.exceptions.ApiException:
            return False

    def copy_cm_to_namespace(self, configmap: V1ObjectMeta,
                             target_namespace: str):
        '''
        clones given k8s configmap to selected namespace
            :param configmap V1ConfigMap: configmap to clone
            :param target_namespace str: destination namespace
            :param label Dict: clone only if filter match
        '''
        sync_label = {self.cm_label: 'yes'}
        configmap = deepcopy(configmap)
        if sync_label.items() <= configmap.metadata.labels.items():
            print(f"sync {configmap.metadata.name} in {target_namespace}")
            configmap.metadata.labels[self.cm_label] = 'insync'
            configmap.metadata.resource_version = None
            configmap.metadata.uid = None
            configmap.metadata.namespace = target_namespace
            if self.is_cm_present(configmap.metadata.name, target_namespace):
                self.api.patch_namespaced_config_map(configmap.metadata.name,
                                                     target_namespace,
                                                     configmap)
            else:
                self.api.create_namespaced_config_map(target_namespace,
                                                      configmap)

    def delete_cm_in_namespace(self, name: str, namespace: str):
        '''
        Delete cm in namespace if label is matched
            :param name: str str: configmap name to delete
        '''
        configmap = self.api.read_namespaced_config_map(
            name,
            namespace,
        )
        sync_label = {self.cm_label: 'insync'}
        if sync_label.items() <= configmap.metadata.labels.items():
            self.api.delete_namespaced_config_map(configmap.metadata.name,
                                                  namespace)

    def sync_new_cm_to_namespaces(self, cm_meta: V1ObjectMeta):
        '''
        copy/update a new cm to managed namespace
            :param cm_meta V1ObjectMeta: newely created configmap metadata
        '''
        target_namespace = self.parse_target_namespaces(cm_meta.namespace)
        try:
            cfg = self.api.read_namespaced_config_map(cm_meta.name,
                                                      cm_meta.namespace)
        except kubernetes.client.rest.ApiException:
            raise exceptions.SourceConfigMap
        for ns in target_namespace:
            self.copy_cm_to_namespace(cfg, ns)

    def sync_all_cms_to_namespace(self, ns_name: str):
        '''
        Copy CM when new namespace is getting created
            :param name: string - new namespace name
        '''
        try:
            all_confmap = self.get_managed_cm()
            for each_ns in all_confmap:
                for configmap in all_confmap[each_ns].items:
                    self.copy_cm_to_namespace(configmap, ns_name)
        except kubernetes.client.rest.ApiException as e:
            print(f"Exception CoreV1Api->list_namespaced_config_map: {e}\n")

    def delete_cm_everywhere(self, cm_meta: V1ObjectMeta):
        '''
        get all the managed namespaces
        get all the matching configmaps
        delete them all
        '''
        try:
            target_namespace = self.parse_target_namespaces(cm_meta.namespace)
            for each_ns in target_namespace:
                print(f'delete {cm_meta.name} in {each_ns}')
                self.delete_cm_in_namespace(cm_meta.name, each_ns)
        except kubernetes.client.rest.ApiException as e:
            print(f"Exception CoreV1Api->list_namespaced_config_map: {e}\n")


class CaBundle(InjectNamespace):
    '''
    Confimap to rapresent an organization ca-certificate
        :param root_ca_file str: file name containing the .crt
        :param configmap_name str: name of the CM containing the .crt
    The resulting configmap will be copied on every slave namespace.
    '''
    def __init__(self,
                 root_ca_file: str,
                 configmap_name: str,
                 cm_label: str = 'pemsync'):
        self.cafile = root_ca_file
        self.camap = configmap_name
        self.cm_label = cm_label
        super().__init__()

    def is_cm_present(self, namespace: str) -> bool:
        try:
            map = self.api.read_namespaced_config_map(self.camap, namespace)
            return self.cafile in map.data.keys()
        except kubernetes.client.exceptions.ApiException:
            return False
