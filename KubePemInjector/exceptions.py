'''
write here unexpected exceptions to work on
'''


class SourceConfigMap(Exception):
    def __init__(self, *args: object) -> None:
        super().__init__(*args)


class UnableToReadSourcePod(Exception):
    def __init__(self, *args: object) -> None:
        super().__init__(*args)


class TimerExited(Exception):
    def __init__(self, *args: object) -> None:
        super().__init__(*args)
