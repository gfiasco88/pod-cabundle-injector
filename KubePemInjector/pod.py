from typing import Dict, List
import time
from .configmap import CaBundle
from . import exceptions
from kubernetes import client

startup = {
    'image': 'update-ca:alpine',
    'imagePullPolicy': 'IfNotPresent',
    'command': [
        '/bin/sh', '-c',
        'sudo update-ca-certificates && sudo cp -r /etc/ssl/certs/* /workdir/'
    ],
    'volumeMounts': None,
    # 'securityContext': {
    #     'runAsUser': 0
    # }
}


class PatchPod(CaBundle):
    '''
    Class to patch A Kubernetes Pod to work with SSLBUMB
        param: root_ca_file - filename containing the cert
        param: configmap - name of the configmap containing file
        param: init_container - init container patching deployments
    '''

    initc_name = 'root-ca-injector-startup'

    def __init__(self,
                 root_ca_file: str,
                 configmap_name: str,
                 init_container: Dict = startup):
        self.master_initc = init_container
        self.master_initc['name'] = self.initc_name
        self.master_initc['volumeMounts'] = [{
            'name': 'volume-ca-bundle',
            'mountPath': '/usr/local/share/ca-certificates/myroot.ca.crt',
            'subPath': root_ca_file,
        }, {
            'name': 'workdir',
            'mountPath': '/workdir'
        }]
        self.volumes = [{
            'name': 'volume-ca-bundle',
            'configMap': {
                'name': configmap_name
            }
        }, {
            'name': 'workdir',
            'emptyDir': {}
        }]
        self.ca_volume_mounts = [{
            'name': 'workdir',
            'mountPath': '/etc/ssl/certs'
        }, {
            'name': 'volume-ca-bundle',
            'mountPath': '/etc/pki/ca-trust/extracted/pem/tls-ca-bundle.pem',
            'subPath': root_ca_file,
        }, {
            'name': 'volume-ca-bundle',
            'mountPath': f'/usr/local/share/ca-certificates/{root_ca_file}',
            'subPath': root_ca_file
        }]
        super().__init__(root_ca_file, configmap_name)

    def load(self, name: str, namespace: str):
        '''
        load V1/pod to patch
            param: name - kubernetes.metadata.name
            param: namespace - kubernetes.metadata.namespace
        '''
        try:
            self.name = name
            self.namespace = namespace
            self.pod = self.api.read_namespaced_pod(name, namespace)
            self.pod_spec = self.pod.spec
            self.metadata = self.pod.metadata
        except client.rest.ApiException:
            raise exceptions.UnableToReadSourcePod

    def watch_namespace(self, namespace: str, body, **__) -> bool:
        '''
        check if deployment namespace is managed by rootca
        check if pem cm are present
        check if pod has't been patched yet
        '''
        labels = body['metadata'].get('labels') or {}
        if namespace in self.managed_namespaces(selector='slave'):
            if labels.get('ca-patched') == 'yes':
                return False
            if self.is_cm_present(namespace):
                return True
        return False

    def patch(self):
        self.metadata.uid = None
        self.metadata.resource_version = None
        self.metadata.labels = self.metadata.labels or {'ca-patched': None}
        self.pod_spec.init_containers = self._extend_none(
            [self.master_initc],
            self.pod_spec.init_containers,
        )
        self.pod_spec.volumes = self._extend_none(
            self.pod_spec.volumes,
            self.volumes,
        )
        for container_list in [
                self.pod_spec.init_containers[1:], self.pod_spec.containers
        ]:
            self._patch_containers(container_list)
        self.metadata.labels['ca-patched'] = 'yes'
        self._deploy()

    def _extend_none(self, item: List, data: List) -> List:
        if (item, data) == (None, None):
            raise Exception('Cannot Concat Empty lists')
        if None in (item, data):
            return data or item
        return item + data

    def _patch_containers(self, container_list: List):
        for cont in container_list:
            cont.volume_mounts = self._extend_none(cont.volume_mounts,
                                                   self.ca_volume_mounts)

    def wait_for_deletion(self, retry: int = 30):
        counter = 0
        while True:
            try:
                self.api.read_namespaced_pod(self.name, self.namespace)
                counter += 1
                time.sleep(1)
            except client.rest.ApiException:
                break
            if counter > retry:
                raise exceptions.TimerExited

    def _deploy(self):
        if self.metadata.generate_name:
            print('LOGGER - this Pod already under k8s management. skipping'
                  )  # skipping rs, deployments etc
            return
        self.api.delete_namespaced_pod(self.name, self.namespace)
        self.wait_for_deletion()
        self.api.create_namespaced_pod(self.namespace, self.pod)


class PatchDeployment(PatchPod):
    '''
    Class to patch A Kubernetes Deployment to work with SSLBUMB
        param: root_ca_file - filename containing the cert
        param: configmap - name of the configmap containing file
    '''
    def __init__(self, root_ca_file: str, configmap_name: str):
        self.apps_v1 = client.AppsV1Api()
        super().__init__(root_ca_file, configmap_name)

    def load(self, name: str, namespace: str):
        '''
        Load V1/apps Deployment to patch
            param: name - kubernetes.metadata.name
            param: namespace - kubernetes.metadata.namespace
        '''
        try:

            self.name = name
            self.namespace = namespace
            self.deployment = self.apps_v1.read_namespaced_deployment(
                name, namespace)
            self.pod_spec = self.deployment.spec.template.spec
            self.metadata = self.deployment.metadata
        except client.rest.ApiException:
            raise exceptions.UnableToReadSourcePod

    def _deploy(self):
        try:
            self.apps_v1.patch_namespaced_deployment(self.name, self.namespace,
                                                     self.deployment)
        except client.rest.ApiException:
            self.apps_v1.replace_namespaced_deployment(self.name,
                                                       self.namespace,
                                                       self.deployment)
