from typing import Optional, List
from kubernetes import client
from contextlib import suppress


class CoreV1:
    def __init__(self):
        self.api = client.CoreV1Api()


class InjectNamespace(CoreV1):

    LABELS_VALUES = ['master', 'slave']
    LABELS = 'rootca-injection'

    def managed_namespaces(self, selector: str = "") -> List[Optional[str]]:
        if selector == "":
            selector = self.LABELS
        elif selector in self.LABELS_VALUES:
            selector = f'{self.LABELS}={selector}'
        else:
            raise ValueError('wrong value for selector')
        namespace_response = self.api.list_namespace(
            label_selector=selector)
        return [nsa.metadata.name for nsa in namespace_response.items]

    def watch_namespace(self, namespace: str, **_) -> bool:
        ''' if there is an update on the master namespaces return true '''
        if namespace in self.managed_namespaces(selector='master'):
            return True
        return False

    def parse_target_namespaces(self, source_namespace: str):
        namespace_list = self.managed_namespaces(selector='slave')
        with suppress(ValueError):
            namespace_list.remove(source_namespace)
        return namespace_list
