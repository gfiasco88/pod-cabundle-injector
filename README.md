# TLS Root CA Injector

**author:** Gian Luca Fiasco  
**mail:** gfiasco88@gmail.com

# Scope: Inject a custom ca-bundle in your containers

in order to achieve this manually the follow tasks need to take place

1. deploy a configMap in your namespace containing the pem file - **WIP**
2. mount the config map on each containers in your deployments/replicaSets/daemonSets

This could be cumbersome to do and requires knowledge by dev on where to get all the tools to work.

# Use Cases

1. make pods work SSLBumb enforced externally (transparent proxy) by your oganization
2. Deploy inside your pods self-signed certificate for services residing outside k8s
3. Deploy organization private cacert inside your pods

# Deploy rootca-injector manually

if you are not a fan of helm here the instruction to deploy it manually

```bash
git clone https://gitlab.lucacloud.info/fiasco/rootca-injector
docker build -f images/Dockerfile -t rootca-injector  .
kubectl apply -f manual/deploy.yaml
```

# Usage

Check the `tests` folder, the injector works as follow:
1. perform actions **only** if two conditions are matched  (can be changed with env variables)
> namespace has been labeled with `rootca-injection` either `master` or `slave`
> ConfigMaps has been labeled with `pemsync=yes` 
2. if a namespace is `rootca-injection=master` and it has got 1 (or more) ConfigMaps `pemsync=yes` then it is the **source** of sync
3. if a namespace is `rootca-injection=slave` then it is in the target namespace list

# Environments Variables

- **CA_CONFIGMAP_NAME** : name of the configmap containing your CAcert
- **CA_CONFIGMAP_FILE** : filename inside your configmap
- **NAMESPACE_WATCH_LABEL**: any namespace with this label will be managed by rootca-injector


# Tests
```
# New Namespace with label "slave" is created
- ConfigMaps are copied
- existing deployments are patched
# New root-ca map is deployed in "master" namespace
- CM is synced
- Deployments are patched
# New Deployment is pushed in slave namespace
- Deployment is patched
```


# TODO
1. init containers need to be rootless (running under unprivilege user)