from setuptools import setup, find_packages
from os import getenv

__version_info__ = (0, 0, 3, '')
__version__ = '{}.{}.{}{}'.format(*__version_info__)

assert __version__ == getenv(
    'CI_COMMIT_TAG', __version__), "version and commit_tag must be the same"

setup(
    name='KubePemInjector',
    version=__version__,
    description='Library to inject pem inside k8s container',
    author='Gian Luca Fiasco',
    author_email='gfiasco88@gmail.com',
    packages=find_packages(exclude=('tests')),
    include_package_data=True,
    url='https://gitlab.lucacloud.info/fiasco/rootca-injector',
    license='Apache 2.0',
    install_requires=['kubernetes==17.17.0', 'kopf==1.31.0'],
)
