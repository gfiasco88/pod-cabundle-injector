import kubernetes as k8s
import kopf
from os import getenv
from KubePemInjector import SyncConfigMap, PatchPod, PatchDeployment
from KubePemInjector.namespace import InjectNamespace

try:
    k8s.config.load_incluster_config()
except k8s.config.config_exception.ConfigException:
    print('Failed to read cluster config - load kubeconfig instead')
    k8s.config.load_kube_config()

pem_map = getenv('CA_CONFIGMAP_NAME', 'ca-pemstore')
pem_file = getenv('CA_CONFIGMAP_FILE', 'myroot.ca.crt')
namespace_label = getenv('NAMESPACE_WATCH_LABEL', 'rootca-injector')
InjectNamespace.LABELS = namespace_label

sync_pem = SyncConfigMap()
inject_pod = PatchPod(configmap_name=pem_map, root_ca_file=pem_file)
inject_deployment = PatchDeployment(configmap_name=pem_map,
                                    root_ca_file=pem_file)


# NameSpace Operations
@kopf.on.create('', 'v1', 'namespaces', labels={namespace_label: 'slave'})
@kopf.on.update('', 'v1', 'namespaces', labels={namespace_label: 'slave'})
def new_namespace_is_created(**kwargs):
    sync_pem.sync_all_cms_to_namespace(kwargs['name'])

    # TODO: namespace_label: 'master' is deleted


# ConfigMap Operations
@kopf.on.create('',
                'v1',
                'configmaps',
                labels={'pemsync': 'yes'},
                when=sync_pem.watch_namespace)
def new_configmap_is_created(**kwargs):
    sync_pem.sync_new_cm_to_namespaces(kwargs['meta'])


@kopf.on.update('',
                'v1',
                'configmaps',
                labels={'pemsync': 'yes'},
                when=sync_pem.watch_namespace)
def configmap_is_updated(**kwargs):
    sync_pem.sync_new_cm_to_namespaces(kwargs['meta'])
    # TODO: reload deployment
    # TODO: relod pods


@kopf.on.delete('',
                'v1',
                'configmaps',
                labels={'pemsync': 'yes'},
                when=sync_pem.watch_namespace)
def rootca_is_deleted(**kwargs):
    sync_pem.delete_cm_everywhere(kwargs['meta'])


# operation on containers
@kopf.on.create('apps',
                'v1',
                'deployments',
                when=inject_deployment.watch_namespace)
@kopf.on.update('apps',
                'v1',
                'deployments',
                when=inject_deployment.watch_namespace)                
def new_deployment_to_patch(**kwargs):
    inject_deployment.load(kwargs['name'], kwargs['namespace'])
    inject_deployment.patch()


@kopf.on.create('', 'v1', 'pods', when=inject_pod.watch_namespace)
def new_pod_to_recreate(**kwargs):
    inject_pod.load(kwargs['name'], kwargs['namespace'])
    inject_pod.patch()
