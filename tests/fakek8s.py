from kubernetes.client import CoreV1Api, AppsV1Api
from kubernetes.client.models import V1ConfigMap
from kubernetes import client

FAKE_PATH = 'tests/fake_responses'


def exception_handler(func):
    def inner_function(*args, **kwargs):
        try:
            func(*args, **kwargs)
        except FileNotFoundError:
            raise client.exceptions.ApiException

    return inner_function


class FakeResponse:
    def __init__(self, filename):
        with open(filename) as file:
            self.data = file.read()


class FakeV1Api:
    def list_namespace(self, label_selector):
        try:
            return CoreV1Api().api_client.deserialize(
                FakeResponse(f'{FAKE_PATH}/name-space-{label_selector}.json'),
                'V1NamespaceList')
        except FileNotFoundError:
            raise client.exceptions.ApiException

    def list_namespaced_config_map(self, namespace, label_selector):
        try:
            return CoreV1Api().api_client.deserialize(
                FakeResponse(
                    f'{FAKE_PATH}/configmap_{namespace}_{label_selector}.json'
                ), 'V1ConfigMapList')
        except FileNotFoundError:
            raise client.exceptions.ApiException

    def read_namespaced_config_map(self, name, namespace):
        try:
            return CoreV1Api().api_client.deserialize(
                FakeResponse(f'{FAKE_PATH}/configmap_{namespace}_{name}.json'),
                'V1ConfigMap')
        except FileNotFoundError:
            raise client.exceptions.ApiException

    def read_namespaced_pod(self, name, namespace):
        try:
            return CoreV1Api().api_client.deserialize(
                FakeResponse(f'{FAKE_PATH}/pod_{namespace}_{name}.json'),
                'V1Pod')
        except FileNotFoundError:
            raise client.exceptions.ApiException

    def patch_namespaced_config_map(self, name, namespace, configmap):
        assert type(name) is str
        assert type(namespace) is str
        assert type(configmap) is V1ConfigMap
        return None

    def create_namespaced_config_map(self, namespace, configmap):
        assert type(namespace) is str
        assert type(configmap) is V1ConfigMap

    def delete_namespaced_config_map(self, name, namespace):
        assert type(name) is str
        assert type(namespace) is str

    def delete_namespaced_pod(self, name, namespace):
        assert type(name) is str
        assert type(namespace) is str


class FakeAppsV1Api:
    def read_namespaced_deployment(self, name, namespace):
        try:
            return AppsV1Api().api_client.deserialize(
                FakeResponse(
                    f'{FAKE_PATH}/deployments_{namespace}_{name}.json'),
                'V1Deployment',
            )
        except FileNotFoundError:
            raise client.exceptions.ApiException

    def patch_namespaced_deployment(self, name, namespace, deployment):
        return
