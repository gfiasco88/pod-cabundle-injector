from KubePemInjector.namespace import InjectNamespace
from fakek8s import FakeV1Api
import pytest


NS = InjectNamespace()
NS.api = FakeV1Api()


def test_watch_namespace():
    assert NS.watch_namespace('skip-ns') is False
    assert NS.watch_namespace('pem-injector') is True
    assert NS.watch_namespace('myproject') is False


def test_managed_namespaces():
    all_ns_name = NS.managed_namespaces()
    assert all_ns_name == ['myproject', 'pem-injector']
    slave_ns_name = NS.managed_namespaces(selector='slave')
    assert slave_ns_name == ['myproject', 'myproject2']
    with pytest.raises(ValueError) as e:
        NS.managed_namespaces(selector='dummy')
        assert str(e.value) == 'wrong value for selector'


def test_parse_target_namespaces():
    target_ns = NS.parse_target_namespaces('pem-injector')
    assert target_ns == ['myproject', 'myproject2']
